//Componente que renderizará los dias del calendario
import datosjson from '../calendario.json'
import Dias_Calendario2 from './Dias_Calendario2'

const Dias_Calendario = (props) => {
    return (
        <>
            {
                // RECORRE EL JSON PARA CREAR UNA CELDA POR CADA DÍA Y DENTRO HACE LLAMADA AL COMPONENTE QUE RENDERIZARÁ EL BOTÓN 
                datosjson.datos.map((dias, i) => {
                    switch (String(dias.fecha).substr(-4, 2)) {
                        case '01':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-t-2 border-r-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias_Calendario2 numero_trabajador={props.numero_trabajador} mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '02':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-t-2 border-r-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias_Calendario2 numero_trabajador={props.numero_trabajador} mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '03':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-t-2 border-r-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias_Calendario2 numero_trabajador={props.numero_trabajador} mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '04':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-t-2 border-r-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias_Calendario2 numero_trabajador={props.numero_trabajador} mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '05':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-t-2 border-r-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias_Calendario2 numero_trabajador={props.numero_trabajador} mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '06':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-t-2 border-r-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias_Calendario2 numero_trabajador={props.numero_trabajador} mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '07':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-t-2 border-r-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias_Calendario2 numero_trabajador={props.numero_trabajador} mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '08':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-t-2 border-r-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias_Calendario2 numero_trabajador={props.numero_trabajador} mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '09':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-t-2 border-r-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias_Calendario2 numero_trabajador={props.numero_trabajador} mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '10':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-t-2 border-r-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias_Calendario2 numero_trabajador={props.numero_trabajador} mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '11':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-t-2 border-r-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias_Calendario2 numero_trabajador={props.numero_trabajador} mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '12':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-t-2 border-r-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias_Calendario2 numero_trabajador={props.numero_trabajador} mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                    }
                })
            }
        </>
    )
}

export default Dias_Calendario