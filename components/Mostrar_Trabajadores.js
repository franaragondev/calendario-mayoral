//Componente que renderizará los datos de los trabajadores
import Dias_Calendario from './Dias_Calendario'
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Mostrar_Trabajadores = (props) => {
    return (
        <>
            {
                props.lista.map((lista_aux, i) => {
                    if (props.lista.length != 0) {
                        return (
                            <>
                                <tr className='border-t-2 border-blue-100'>
                                    {
                                        typeof window !== 'undefined' && window.localStorage && localStorage.getItem('listaTrabajadores')
                                            ?
                                            <>
                                            {/* RENDERIZA LOS DATOS DEL TRABAJADOR */}
                                                <td id={i} className='border-l-2 border-t-2 border-r-2 border-blue-100 fixed flex w-96 bg-white p-2'><FontAwesomeIcon className='w-5 mr-4' icon={faUser} />{lista_aux[1]} <span className='ml-4 bg-green-600 text-white pr-2 pl-2'>{lista_aux[0]}/22</span></td>

                                            </>
                                            :
                                            <span></span>
                                    }
                                    {/* RENDERIA LOS DÍAS DEL AÑO PARA CADA TRABAJADOR */}
                                    <Dias_Calendario numero_trabajador={i} />
                                </tr>
                            </>
                        )
                    }
                })
            }
        </>
    )
}

export default Mostrar_Trabajadores