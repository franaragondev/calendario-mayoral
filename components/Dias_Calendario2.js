//Componente que servirá de auxiliar para renderizar los días del calendario
import Swal from 'sweetalert2'

//FUNCIÓN QUE SE EJECUTARÁ CUANDO SE CLICA UN BOTÓN
function clicado(id, numero_trabajador, color) {
    if (color == 'BLANCO') {
        //SUMA NÚMERO DE VACACIONES SELECCIONADAS
        if (typeof window !== 'undefined' && window.localStorage && localStorage.getItem('listaTrabajadores')) {
            let array = JSON.parse(localStorage.getItem('listaTrabajadores'))
            if (array[numero_trabajador][0] + 1 <= 22) {
                array[numero_trabajador][0] = array[numero_trabajador][0] + 1
                window.location.href = 'http://localhost:3000/'
                // ACTUALIZA RELACIÓN DEL TRABAJADOR Y EL DÍA EN EL JSON
                array[numero_trabajador][2].map((array_aux, i) => {
                    if (String(array_aux.fecha) == String(id).substr(0, 8)) {
                        array[numero_trabajador][2][i].color = 'VERDE'
                    }
                })
                // ACTUALIZA JSON
                localStorage.setItem('listaTrabajadores', JSON.stringify(array))
                //AVISA AL USUARIO QUE YA HA SELECCIONADO EL NÚMERO MÁXIMO DE DIAS
            } else {
                Swal.fire({
                    title: `El número máximo de vacaciones ya se ha seleccionado`,
                    confirmButtonColor: '#6E7D88',
                    confirmButtonText: 'Cerrar'
                })
            }
        }

    } else {
        //RESTA NÚMERO DE VACACIONES SELECCIONADAS
        if (typeof window !== 'undefined' && window.localStorage && localStorage.getItem('listaTrabajadores')) {
            let array = JSON.parse(localStorage.getItem('listaTrabajadores'))
            array[numero_trabajador][0] = array[numero_trabajador][0] - 1
            // ACTUALIZA RELACIÓN DEL TRABAJADOR Y EL DÍA EN EL JSON
            array[numero_trabajador][2].map((array_aux, i) => {
                if (String(array_aux.fecha) == String(id).substr(0, 8)) {
                    array[numero_trabajador][2][i].color = 'BLANCO'
                }
            })
            // ACTUALIZA JSON
            localStorage.setItem('listaTrabajadores', JSON.stringify(array))
            window.location.href = 'http://localhost:3000/'
        }
    }

}

const Dias_Calendario2 = (props) => {
    let array = JSON.parse(localStorage.getItem('listaTrabajadores'))
    return (
        <>
            {
                typeof window !== 'undefined' && window.localStorage && localStorage.getItem('listaTrabajadores')
                    ?
                    array[props.numero_trabajador][2].map((dias_aux) => {
                        if (String(dias_aux.fecha).substr(-4, 2) == props.mes && props.year == String(dias_aux.fecha).substr(0, 4)) {
                            // DEPENDIENDO DEL COLOR DEL BOTÓN SE RENDERIZARÁ UNO U OTRO
                            switch (dias_aux.color) {
                                case 'AZUL':
                                    return <td key={dias_aux.fecha} className='border-2 m-1 border-white'>
                                        <button className='cursor-default border-2 m-1 border-blue-300 text-blue-600 bg-blue-600'>{String(dias_aux.fecha).substr(-2)}</button>
                                    </td>
                                    break;
                                case 'ROJO':
                                    return <td key={dias_aux.fecha} className='border-2 m-1 border-white'>
                                        <button className='cursor-default border-2 m-1 border-blue-300 text-red-600 bg-red-600'>{String(dias_aux.fecha).substr(-2)}</button>
                                    </td>
                                    break;
                                case 'BLANCO':
                                    return <td key={dias_aux.fecha} className='border-2 m-1 border-white'>
                                        <button id={String(dias_aux.fecha) + '-' + String(props.numero_trabajador)} onClick={() => clicado(String(dias_aux.fecha) + '-' + String(props.numero_trabajador), String(props.numero_trabajador), dias_aux.color)} className='border-2 m-1 border-blue-300 text-white'>{String(dias_aux.fecha).substr(-2)}</button>
                                    </td>
                                    break;
                                case 'VERDE':
                                    return <td key={dias_aux.fecha} className='border-2 m-1 border-white'>
                                        <button id={String(dias_aux.fecha) + '-' + String(props.numero_trabajador)} onClick={() => clicado(String(dias_aux.fecha) + '-' + String(props.numero_trabajador), String(props.numero_trabajador), dias_aux.color)} className='border-2 m-1 border-blue-300 text-green-600 bg-green-600'>{String(dias_aux.fecha).substr(-2)}</button>
                                    </td>
                                    break;
                            }

                        }
                    })
                    :
                    <span></span>
            }
        </>
    )
}

export default Dias_Calendario2