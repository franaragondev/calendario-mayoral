//Componente que renderizará los nombres de los meses del calendario
import datosjson from '../calendario.json'

const Meses = (props) => {

    return (
        <>
            {
                datosjson.datos.map((meses) => {
                    // RENDERIZA LAS CELDAS QUE CONTENDRÁN EL NOMBRE DE LOS MESES
                    switch (String(meses.fecha).substr(-4, 2)) {
                        case '01':
                            if (String(meses.fecha).substr(-2) == '01') {
                                return (<td className='bg-gray-300 m-1 text-center' key={meses.fecha}>ENERO</td>)
                            }
                            break;
                        case '02':
                            if (String(meses.fecha).substr(-2) == '01') {
                                return (<td className='bg-gray-400 m-1 text-center' key={meses.fecha}>FEBRERO</td>)
                            }
                            break;
                        case '03':
                            if (String(meses.fecha).substr(-2) == '01') {
                                return (<td className='bg-gray-300 m-1 text-center' key={meses.fecha}>MARZO</td>)
                            }
                            break;
                        case '04':
                            if (String(meses.fecha).substr(-2) == '01') {
                                return (<td className='bg-gray-400 m-1 text-center' key={meses.fecha}>ABRIL</td>)
                            }
                            break;
                        case '05':
                            if (String(meses.fecha).substr(-2) == '01') {
                                return (<td className='bg-gray-300 m-1 text-center' key={meses.fecha}>MAYO</td>)
                            }
                            break;
                        case '06':
                            if (String(meses.fecha).substr(-2) == '01') {
                                return (<td className='bg-gray-400 m-1 text-center' key={meses.fecha}>JUNIO</td>)
                            }
                            break;
                        case '07':
                            if (String(meses.fecha).substr(-2) == '01') {
                                return (<td className='bg-gray-300 m-1 text-center' key={meses.fecha}>JULIO</td>)
                            }
                            break;
                        case '08':
                            if (String(meses.fecha).substr(-2) == '01') {
                                return (<td className='bg-gray-400 m-1 text-center' key={meses.fecha}>AGOSTO</td>)
                            }
                            break;
                        case '09':
                            if (String(meses.fecha).substr(-2) == '01') {
                                return (<td className='bg-gray-300 m-1 text-center' key={meses.fecha}>SEPTIEMBRE</td>)
                            }
                            break;
                        case '10':
                            if (String(meses.fecha).substr(-2) == '01') {
                                return (<td className='bg-gray-400 m-1 text-center' key={meses.fecha}>OCTUBRE</td>)
                            }
                            break;
                        case '11':
                            if (String(meses.fecha).substr(-2) == '01') {
                                return (<td className='bg-gray-300 m-1 text-center' key={meses.fecha}>NOVIEMBRE</td>)
                            }
                            break;
                        case '12':
                            if (String(meses.fecha).substr(-2) == '01') {
                                return (<td className='bg-gray-400 m-1 text-center' key={meses.fecha}>DICIEMBRE</td>)
                            }
                            break;
                    }
                })
            }
        </>
    )
}

export default Meses