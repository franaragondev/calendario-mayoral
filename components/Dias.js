//Componente que renderizará los dias del calendario que se mostrarán debajo de los meses
import datosjson from '../calendario.json'
import Dias2 from './Dias2'

const Dias = (props) => {

    return (
        <>
            {
                // POR CADA MES REENDERIZA LOS DÍAS CORRESPONDIENTES
                datosjson.datos.map((dias) => {
                    switch (String(dias.fecha).substr(-4, 2)) {
                        case '01':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-r-2 border-b-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias2 mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '02':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-r-2 border-b-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias2 mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '03':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-r-2 border-b-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias2 mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '04':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-r-2 border-b-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias2 mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '05':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-r-2 border-b-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias2 mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '06':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-r-2 border-b-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias2 mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '07':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-r-2 border-b-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias2 mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '08':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-r-2 border-b-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias2 mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '09':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-r-2 border-b-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias2 mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '10':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-r-2 border-b-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias2 mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '11':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-r-2 border-b-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias2 mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                        case '12':
                            if (String(dias.fecha).substr(-2) == '01') {
                                return (
                                    <td className='border-r-2 border-b-2 border-blue-100 m-1' key={dias.fecha}>
                                        <Dias2 mes={String(dias.fecha).substr(-4, 2)} year={String(dias.fecha).substr(0, 4)} />
                                    </td>
                                )
                            }
                            break;
                    }
                })
            }
        </>
    )
}

export default Dias