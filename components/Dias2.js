//Componente que renderizará los botones para los dias del calendario que se mostrarán debajo de los meses
import datosjson from '../calendario.json'

const Dias2 = (props) => {

    return (
        <>
            {
                datosjson.datos.map((dias_aux) => {
                    //DEPENDIENDO DEL TIPO DE DÍA SE RENDERIZARÁ EL BOTÓN DE UN COLOR U OTRO
                    if(String(dias_aux.fecha).substr(-4, 2) == props.mes && props.year == String(dias_aux.fecha).substr(0, 4)){
                        switch (dias_aux.color) {
                            case 'AZUL':
                                return <td key={dias_aux.fecha} className='border-2 m-1 border-white'>
                                    <button className='cursor-default border-2 m-1 border-blue-300 text-white bg-blue-600'>{String(dias_aux.fecha).substr(-2)}</button>
                                </td>
                                break;
                            case 'ROJO':
                                return <td key={dias_aux.fecha} className='border-2 m-1 border-white'>
                                    <button className='cursor-default border-2 m-1 border-blue-300 text-white bg-red-600'>{String(dias_aux.fecha).substr(-2)}</button>
                                </td>
                                break;
                            case 'BLANCO':
                                return <td key={dias_aux.fecha} className='border-2 m-1 border-white'>
                                    <button className='cursor-default border-2 m-1 border-blue-300'>{String(dias_aux.fecha).substr(-2)}</button>
                                </td>
                                break;
                        }
                    }
                })
            }
        </>
    )
}

export default Dias2